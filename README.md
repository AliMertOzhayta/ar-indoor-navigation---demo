# AR Indoor Navigation - Demo



Augmented Reality Indoor Navigation App is powered by ARKit and Placenote frameworks without using <b>GPS</b>, <b>bluetooth</b> or <b>WiFi</b> beacons.


![](demo/AR_Indoor_Navigation_App_Demo.mp4)



<a href="https://www.dropbox.com/s/6ii13kl42a6epiv/AR%20Indoor%20Navigation%20App%20Demo.mp4?dl=0">If you can't view the video, please press here..</a>

# Source codes are in the private repository!
